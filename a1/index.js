/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
function userInfo(){
    let fullName = prompt("Enter your Full Name here.");
    let userAge = prompt("Enter your age here.");
    let userAddress = prompt("Enter your address here.");

    console.log(`Hello, ${fullName}`);
    console.log(`You are ${userAge} years old.`);
    console.log(`You live in ${userAddress}`);
}

userInfo();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function faveArtists(){
    let firstArtist = prompt("Enter your favorite singer or musical artist here");
    let secondArtist = prompt("Enter your 2nd favorite singer or musical artist here");
    let thirdArtist = prompt("Enter your 3rd favorite singer or musical artist here");
    let fourthArtist = prompt("Enter your 4th favorite singer or musical artist here");
    let fifthArtist = prompt("Enter your 5th favorite singer or musical artist here");

    console.log(`1. ${firstArtist}`);
    console.log(`2. ${secondArtist}`);
    console.log(`3. ${thirdArtist}`);
    console.log(`4. ${fourthArtist}`);
    console.log(`5. ${fifthArtist}`);
}

faveArtists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function faveMovies(){
    let firstMovie = "Drive My Car";
    let firstMovieRating = "97%";
    console.log(`1. ${firstMovie}`);
    console.log(`Rotten Tomatoes Rating: ${firstMovieRating}`)

    let secondMovie = "Birdman";
    let secondMovieRating = "91%";
    console.log(`2. ${secondMovie}`);
    console.log(`Rotten Tomatoes Rating: ${secondMovieRating}`)

    let thirdMovie = "5 Centimeters per Second";
    let thirdMovieRating = "88%";
    console.log(`3. ${thirdMovie}`);
    console.log(`Rotten Tomatoes Rating: ${thirdMovieRating}`)

    let fourthMovie = "2001: A Space Odyssey";
    let fourthMovieRating = "92%";
    console.log(`4. ${fourthMovie}`);
    console.log(`Rotten Tomatoes Rating: ${fourthMovieRating}`)

    let fifthMovie = "Perfect Blue";
    let fifthMovieRating = "83%";
    console.log(`5. ${fifthMovie}`);
    console.log(`Rotten Tomatoes Rating: ${fifthMovieRating}`)
}

faveMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();

// console.log(friend1);
// console.log(friend2);